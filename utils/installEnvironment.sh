#!/bin/bash

apt update;

apt install \
	minidlna \
	samba \
	samba-client \
	sway \
	firefox-esr \
	curl \
	lm-sensors \
	qbittorrent-nox \
	transmission-cli \
	transmission-daemon \
	libapache2-mod-php \
	avahi-daemon \
	libnss-mdns \
	php-cgi \
	libjs-jquery \
	libjs-bootstrap5 \
	libjs-bootstrap \
	libjs-bootstrap4 \

systemctl enable minidlna;
systemctl enable samba;
systemctl enable transmission-daemon;
systemctl enable avahi-daemon;
systemctl enable lighttpd;

softLinkHTML=("bootstrap" "bootstrap4" "bootstrap5" "jquery")
baseDirectoryScriptHTML="/usr/share/javascript/"

for link in ${softLinkHTML[@]}; do

if [ ! -d "$link" ]; then
 echo "Creating link to $link"
 ln -s "$baseDirectoryScriptHTML$link" ./

fi

done


fileTransmissionUser="/etc/systemd/system/transmission-daemon.service.d/run-as-user.conf"
if [ ! -e "$fileTransmissionUser" ]; then

echo "Setting user for transmission daemon"
echo "[Service]" >>  "/etc/systemd/system/transmission-daemon.service.d/run-as-user.conf"
echo "User=mydns" >>  "/etc/systemd/system/transmission-daemon.service.d/run-as-user.conf"

fi

