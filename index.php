<!DOCTYPE html>

<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <link href="/bootstrap5/css/bootstrap.min.css" rel="stylesheet">
  <script src="/bootstrap5/js/bootstrap.bundle.min.js"></script>

  <title>myDNS</title>
</head>

<body>
  <div class="container-fluid">

    <div class="row">
      <div class="col-12">
        <h1 class="display-3" style="text-align: center">
          MyDNS
      </div>
      <hr>

      <div class="col-12">
        <h2 style="text-align: center">
          Server stats
        </h2>
        <?php
        $prefix = "<pre>\n";
        $suffix = "\n</pre>";
        ?>

        <div class="col-12">
          <ul class="nav nav-tabs justify-content-center" role="tablist" style="align-content: center;">
            <li class="nav-item">
              <a class="nav-link active" data-bs-toggle="tab" href="#storage">Storage</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-bs-toggle="tab" href="#memory">Memory</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-bs-toggle="tab" href="#sensors">Sensors</a>
            </li>
          </ul>

          <div class="tab-content">
            <div id="storage" class="container tab-pane active">
              <br>
              <?php
              echo $prefix . shell_exec('df -h') . $suffix;
              ?>
            </div>

            <div id="memory" class="container tab-pane fade">
              <br>
              <?php
              echo $prefix . shell_exec('free -m') . $suffix;
              ?>
            </div>

            <div id="sensors" class="container tab-pane fade">
              <br>
              <?php
              echo $prefix . shell_exec('sensors') . $suffix;
              ?>
            </div>
          </div>
        </div>
        <hr>
      </div>

      <div class="col-sm-3">
        <div class="card" style="padding: 20px;">
          <img class="card-img-top" src="images/pihole.png" alt="PiHole">
          <div class="card-body">
            <center>
              <hr>
              <!--<h4 class="card-title">PiHole</h4>-->
              <a href="/admin/" class="btn btn-primary" style="text-align: center;">PiHole</a>
            </center>
          </div>
        </div>
      </div>

      <div class="col-sm-3">
        <div class="card" style="padding: 20px;">
          <img class="card-img-top" src="images/Transmission.jpg" alt="Transmission">
          <div class="card-body">
            <center>
              <hr>
              <!--<h4 class="card-title">Transmission</h4>-->
              <a href="http://<?php print $_SERVER['SERVER_NAME']; ?>:9091" class="btn btn-primary"
                style="text-align: center;">Transmission</a>
            </center>
          </div>
        </div>
      </div>

      <div class="col-sm-3">
        <div class="card" style="padding: 20px;">
          <img class="card-img-top" src="images/qbittorrent.png" alt="QBitTorrent">
          <div class="card-body">
            <center>
              <hr>
              <!--<h4 class="card-title">QBitTorrent</h4>-->
              <a href="http://<?php print $_SERVER['SERVER_NAME']; ?>:8080" class="btn btn-primary"
                style="text-align: center;">QBitTorrent</a>
            </center>
          </div>
        </div>
      </div>

      <div class="col-sm-3">
        <div class="card" style="padding: 20px;">
          <img class="card-img-top" src="images/gerbera.png" alt="Gerbera">
          <div class="card-body">
            <center>
              <hr>
              <!--<h4 class="card-title">Gerbera</h4>-->
              <a href="http://<?php print $_SERVER['SERVER_NAME']; ?>:49152" class="btn btn-primary"
                style="text-align: center;">Gerbera</a>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>